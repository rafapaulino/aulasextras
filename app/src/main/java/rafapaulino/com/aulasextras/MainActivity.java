package rafapaulino.com.aulasextras;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button objetoBotao = (Button) findViewById(R.id.botaozinho);

        objetoBotao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objetoIntent = new Intent(MainActivity.this, SegundaTela.class);
                objetoIntent.putExtra("variavel","Rafael Lindão");
                startActivity(objetoIntent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("LogX","Evento Start");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("LogX","Evento Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("LogX","Evento Destroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("LogX","Evento Pause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("LogX","Evento Resume");
    }
}
